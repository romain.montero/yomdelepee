
public class Main {
    public static void main(String[] args){
        testPrint();
        testPrintErr();
    }

    public static void testPrintErr() {
        HelloWorld.printHelloWorldErr();
    }

    public static void testPrint() {
        new HelloWorld().printHelloWorld();
    }
}
