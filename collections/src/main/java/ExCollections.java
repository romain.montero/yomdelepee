import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExCollections {
    public static void wordCount(String input) {
        Map<String, Long> words = Stream.of(input.split(" "))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        words.entrySet()
            .stream()
            .sorted(Map.Entry.comparingByKey())
            .map(entry -> entry.getKey() + ": " + entry.getValue())
            .forEach(System.out::println);
    }

    public static void allPalindromes(String input) {
        Set<String> substrings = new HashSet<>();
        for (int i = 0; i < input.length(); i++) {
            for (int j = i+1; j <= input.length(); j++) {
                String substring = input.substring(i,j);
                if(isPalindrome(substring)) {
                    substrings.add(substring);
                }
            }
        }
        substrings.stream().sorted().forEach(System.out::println);
    }

    private static boolean isPalindrome(String s) {
        int n = s.length();
        for (int i = 0; i < (n/2); i++) {
            if (s.charAt(i) != s.charAt(n - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
