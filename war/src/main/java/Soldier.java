public class Soldier implements Combat {
    int hp;
    int dp;
    String scream;

    public Soldier() {
        hp = 15;
        dp = 3;
        scream = "No pity for losers!";
    }

    public void kill() {
        hp = 0;
    }

    @Override
    public void printState() {
        System.out.println("I have " + hp +" health points.");
    }

    @Override
    public void attack(Soldier s) {
        s.hp -= dp;
    }

    @Override
    public void attack(Vehicle v) {
        System.out.println("I can't fight this");
    }

    @Override
    public void scream() {
        System.out.println(scream);
    }
}
