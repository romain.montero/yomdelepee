public interface Combat {
    default void printState() {
        System.out.println("Error 404. Class not found.");
    }
    void attack(Soldier s);
    void attack(Vehicle v);
    void scream();
}
