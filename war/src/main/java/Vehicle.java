public class Vehicle implements Combat {
    String name;
    int defense;

    public Vehicle(String name, int defense) {
        this.name = name;
        this.defense = defense;
    }

    @Override
    public void printState() {
        System.out.println("I have " + defense + " defense points.");
    }

    @Override
    public void attack(Soldier s) {
        s.hp = 0;
    }

    @Override
    public void attack(Vehicle v) {
        v.defense /= 2;
    }

    @Override
    public void scream() {
        System.out.println("I'm " + name + "!");
    }
}
